//------------------------------------------------------------------------------
// m5272lcd.c
//
//   Funciones de configuraci�n y control de un LCD UC-12202-A SAMSUNG 
//   conectado a la plataforma de desarrollo ENT2004CF
//
// Autor: Javier Guill�n �lvarez
//------------------------------------------------------------------------------
#ifndef __M5272LCD_C__
#define __M5272LCD_C__

#include "m5272lib.h"
#include "m5272gpio.c"
#include "stdarg.h"
#include "printf.h"
//------------------------------------------------------------------
// TIPO:        LCD_TIPO
// DESCRIPCI�N: sirve para indicar a LCD_write si la se le pasa
//		un car�cter para imprimir o una instrucci�n
//------------------------------------------------------------------
typedef enum {LCD_CHAR = 0x40, LCD_INST = 0x00} LCD_TIPO;

#define CLR_DISP	0x0100		// Clear Display
#define LIN_1LCD	0x8000		// Set DRAM dir=0x00 (Inicio 1� l�nea)
#define LIN_2LCD	0xC000		// Set DRAM dir=0x40 (Inicio 2� l�nea)
#define FS_8BITS	0x3000		// Function Set para 8 bits
#define FS_CONF8	0x3800		// Function Set: 8bits, 2l�neas, 5x7puntos
#define MODE_SET	0x0600		// Mode Set: autoincremento, no shift
#define CD_ON		0x0F00		// Display ON, Cursor ON, Parpadeo ON
#define BIT_ENABLE	0x0080

#define TAM_BUFFER 40
#define DISPLAY_SHIFT_RIGHT 0X1C00 //	Mover el texto a la derecha en el buffer (de 40 caracteres por linea)
#define DISPLAY_SHIFT_LEFT 0X1800 //	Mover el texto a la izquierda en el buffer (de 40 caracteres por linea)
#define RETURN_HOME 0X0002	//Volver al principio del buffer
// MACROS PARA ABREVIAR LAS LLAMADAS A LCD_write
#define LCD_dato(dato) LCD_write(dato, LCD_CHAR)
#define LCD_inst(inst) LCD_write(inst, LCD_INST)
/* conversi�n a min�sculas de una letra */
#define MINUSCULAS(c)		(((c >= 'A') && (c <= 'Z'))? c+= 'a'-'A' : c)
//------------------------------------------------------
// void LCD_write(UWORD dato, LCD_TIPO tipo)
//
// Descripci�n:
//   Funci�n para escribir en el LCD
//
// Par�metros:
//   char dato
//     instrucci�n o car�cter a escribir en el display
//   LCD_TIPO tipo
//     LCD_CHAR si dato es un car�cter a escribir
//     LCD_INST si dato es una instrucci�n para el LCD
//------------------------------------------------------
void LCD_write(UWORD dato, LCD_TIPO tipo)
{
  if(tipo == LCD_CHAR){
    dato = dato << 8;				// El dato debe estar en los 8 bits m�s significativos
  }
  set16_puertoS(dato | tipo | BIT_ENABLE);	// Enable + dato: Activamos LCD e indicamos el tipo de dato
  retardo(RET_3MS);
  set16_puertoS(dato | tipo);			// Disable: carga el dato en el LCD
  retardo(RET_3MS);
}

//--------------------------------------
// void LCD_init()
//
// Descripci�n:
//   Funci�n de inicializaci�n del LCD
//--------------------------------------
void LCD_init()
{
  LCD_inst(FS_CONF8);	// Function Set: 8bits, 2l�neas, 5x7puntos
  LCD_inst(CLR_DISP);	// Clear Display 
  LCD_inst(MODE_SET);	// Autoincremento del cursor y sin desplazamiento (shift)
  LCD_inst(CD_ON);	// Activa el display, el cursor y el parpadeo
}

//--------------------------------------
// void LCD_reset()
//
// Descripci�n:
//   Funci�n de inicializaci�n del LCD
//--------------------------------------
void LCD_reset()
{
  int i;
  retardo(RET_15MS);		// Retardo inicial
  for(i = 0; i < 3; i++){	// Se repite 3 veces,
    LCD_inst(FS_8BITS);		//   Instrucci�n para funcionamiento en 8 bits
    retardo(RET_15MS);		//   Hay que esperar m�s de 4.1ms
  }
}

int impresos=0;


void LCD_desplazar(int impresos){
	int i=0;
	while(1){
			for(i=0;i<28;i++){
			LCD_inst(DISPLAY_SHIFT_LEFT);
			retardo(200000);
			}
		}
}

/*--------------------------------------
Descripci�n: 
	Funci�n para imprimir cadenas en el LCD
----------------------------------------*/
void LCD_string(char *string){
	
	while(*(string)!='\0' && impresos <=73){ //dejamos 3 blancos para que el mensaje no sea continuo
		if(impresos==37){
			LCD_inst(LIN_2LCD);
		}	
		LCD_dato(*string++);
		impresos++;
		_printf("%d ",impresos);
	}
	if (impresos > 12){
		LCD_desplazar(impresos);
	}
	impresos=0;
}


/*-------------------------------------
Descripci�n:
	Funci�n para imprimir n�meros en base decimal
	por el LCD
---------------------------------------*/
void LCD_outNum10(long num){
  char* chars = "0123456789";

  char *p, buf[34];
  unsigned long x;

  if (num < 0){
      LCD_dato('-');
      x = -num;
  }else
    x = num;
  
  p = buf + sizeof (buf);
  *--p = '\0';
do{
	*--p = chars[x % 10];
    x /= 10;
  }while (x != 0);
  LCD_string(p);
}


/*==============================*/
void LCD_printf(char *formato,...)
/*==============================*/
/*
POR HACER
1. Si se han escrito 12 caracteres en la linea, se salta a la siguiente.
2. si se han escrito 12 caracteres en la segunda linea, se carga la siguiente o se borra la pantalla.
   void cambiarLinea(imprimidos);
   el buffer se puede hacer como char* buffer[numero de lineas][12]; y que en vez de escribir en el lcd,
  se escriba en el buffer y se muestren una ventana de 2 l�neas. Podr�amos programar un caracter de ^ o uno de abajo
*/
{
	/* tipos simples que soporta LCD_printf */
//	char caracter;
	char *texto;
	int entero;

	va_list args;
    LCD_inst(CLR_DISP);		//     Limpiamos el display
    LCD_inst(LIN_1LCD);		//     y volvemos a la l�nea 1



/* le indicamos que el �ltimo argumento fijo es formato */
va_start(args,formato);

/* recorremos toda la cadena de formato */
while (*formato != '\0') {

	/* si encontramos indicios de querer representar un tipo */
	if (*formato == '%') {
		formato++;
	
		/* lo convertimos a min�sculas */
		switch (MINUSCULAS(*formato)) {
			/* int */
			case 'd':
			case 'i':	
					entero = va_arg(args,int);
					LCD_outNum10(entero);
					break;

			//char 
			/*
			case 'c':	caracter = va_arg(args,char);
					LCD_dato(caracter);
					break;
					*/
			
			/* char */

			case 's':	
					texto = va_arg(args,char*);
					LCD_string(texto);
					break;
			/* s�mbolo % */
			case '%':	LCD_dato(*formato);
					break;
			}
		}
	/* si no representamos el caracter */
	else
	LCD_dato(*formato);
	//LCD_string(texto);
	formato++;
	}
/* fin de los argumentos */
va_end(args);

return;
}


#endif